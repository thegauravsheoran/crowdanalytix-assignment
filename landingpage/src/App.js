import React from 'react';
import Header from './components/Header';
import Features from './components/Features';
import ProductGallery from './components/ProductGallery';
import Testimonials from './components/Testimonials';
import CallToAction from './components/CallToAction';

const App = () => {
  return (
    <div>
      <Header />
      <Features />
      <ProductGallery />
      <Testimonials />
      <CallToAction />
    </div>
  );
}

export default App;
