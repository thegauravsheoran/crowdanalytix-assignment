import React from 'react';
import { MdSecurity, MdNotificationsActive, MdBatteryChargingFull } from 'react-icons/md';

const Features = () => {
  return (<>
    <section className="features" id="features">
      <div className="features__item">
        <MdSecurity className="features__icon" />
        <h3 className="features__title">Advanced Security</h3>
        <p className="features__description">Our new mobile phone is equipped with the latest security features to keep your data safe and secure.</p>
      </div>
      <div className="features__item">
        <MdNotificationsActive className="features__icon" />
        <h3 className="features__title">Smart Notifications</h3>
        <p className="features__description">Stay on top of your notifications with our intelligent notification system that learns your preferences and behavior.</p>
      </div>
      <div className="features__item">
        <MdBatteryChargingFull className="features__icon" />
        <h3 className="features__title">Fast Charging</h3>
        <p className="features__description">Never run out of battery again with our fast charging technology that can charge your phone in minutes.</p>
      </div>
    </section>
    <style>
        {`
        .features {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-items: center;
            padding: 50px;
            background-color: #f5f5f5;
          }
          
          .features__item {
            width: 33.33%;
            padding: 20px;
            text-align: center;
          }
          
          .features__icon {
            font-size: 48px;
            color: #2196f3;
            margin-bottom: 20px;
          }
          
          .features__title {
            font-size: 24px;
            margin-bottom: 10px;
          }
          
          .features__description {
            font-size: 18px;
            line-height: 1.5;
          }
          
        `}
        </style>
    </>    

  );
}

export default Features;
