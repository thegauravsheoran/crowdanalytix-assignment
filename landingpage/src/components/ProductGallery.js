import React from 'react';

const ProductGallery = () => {


  return (<>
    <section className="product-gallery" id="product-gallery">
      <div className="product-gallery__item">
        <img src="https://www.pngall.com/wp-content/uploads/5/Bluetooth-Headset-PNG.png" alt="Product 1" className="product-gallery__image" />
        <div className="product-gallery__info">
          <h3 className="product-gallery__name">Wireless Headphones</h3>
          <span className="product-gallery__price">$99.99</span>
          <a href="#" className="product-gallery__button">Buy Now</a>
        </div>
      </div>
      <div className="product-gallery__item">
        <img src="https://m.media-amazon.com/images/I/71yzJoE7WlL._SX522_.jpg" alt="Product 1" className="product-gallery__image" />
        <div className="product-gallery__info">
          <h3 className="product-gallery__name">Iphone 14 Pro Max</h3>
          <span className="product-gallery__price">$1229.99</span>
          <a href="#" className="product-gallery__button">Buy Now</a>
        </div>
      </div>
      <div className="product-gallery__item">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3ltg9bi4V2kjCmzSv8m3A3KA8elSsJNRx5gC7VbwgVBZkeKmNyntif95oV8K3yTv6pCs&usqp=CAU" alt="Product 3" className="product-gallery__image" />
        <div className="product-gallery__info">
          <h3 className="product-gallery__name">Airpods</h3>
          <span className="product-gallery__price">$149.99</span>
          <a href="#" className="product-gallery__button">Buy Now</a>
        </div>
      </div>
      <div className="product-gallery__item">
        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXg9mjnWwfcXgBozBU4mK1RU2mQ_V5cbbZOpMLy7BuDK3r45iFxeSBa0wZsAKGHMK2PLg&usqp=CAU" alt="Product 4" className="product-gallery__image" />
        <div className="product-gallery__info">
          <h3 className="product-gallery__name">Samsung S21 Ultra</h3>
          <span className="product-gallery__price">$1999.99</span>
          <a href="#" className="product-gallery__button">Buy Now</a>
        </div>
      </div>
    </section>
    <style>{`
    .product-gallery {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: center;
        padding: 50px;
        background-color: #fff;
      }
      
      .product-gallery__item {
        width: 48%;
        margin-bottom: 30px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
      }
      
      .product-gallery__image {
        width: 50%;
        height: 370px;
        margin-left:150px;
        object-fit: cover;
      }
      
      .product-gallery__info {
        padding: 20px;
      }
      
      .product-gallery__name {
        font-size: 24px;
        margin-bottom: 10px;
      }
      
      .product-gallery__price {
        font-size: 18px;
        font-weight: bold;
      }
      
      .product-gallery__button {
        display: inline-block;
        padding: 10px 20px;
        background-color: #2196f3;
        color: #fff;
        text-decoration: none;
        border-radius: 5px;
        margin-top: 10px;
        margin-left:400px;
      }
      .product-gallery__button:hover{
        background-color:blue;
      }
    `}</style>
    </>
  );
}

export default ProductGallery;
