import React from 'react';

const Header = () => {
  return (
    <>
    <header>
      <nav>
        <ul>
          <li><a href="#header">Home</a></li>
          <li><a href="#features">Features</a></li>
          <li><a href="#product-gallery">Gallery</a></li>
          <li><a href="#testimonials">Testimonials</a></li>
          <li><a href="#call-to-action">Order Now</a></li>
        </ul>
      </nav>
      </header>
      <div className="header__content">
        <h1>New Mobile Phone and Bluetooth Headset Launch</h1>
        <img  className="header__content1" src="https://www.pngall.com/wp-content/uploads/5/Bluetooth-Headset-PNG.png" alt="New Bluetooth Headset" />
        <img  className="header__content2"src="https://m.media-amazon.com/images/I/71yzJoE7WlL._SX522_.jpg" alt="New Iphone 14 pro max"/>
      </div>
    
    <style>
        {`
            header {
                display: flex;
                justify-content: space-between;
                align-items: center;
                padding: 20px;
                background-color: #f0f0f0;
              }
              
              nav ul {
                display: flex;
                float:right;
                list-style: none;
                padding: 0;
                margin-left:900px;
              }
              
              nav ul li {
              float:right;
              margin: 0 20px;
             
              }
              
              nav ul li a {
               
                float:right;
                color: #333;
                text-decoration: none;
                font-size: 18px;
              }
              
              .header__content {
                display: inline;
                flex-direction: column;
                align-items: right;
                text-align: center;
                
              }
              .header__content h1 {
                font-size: 40px;
                margin: 20px 0;
              }
              
              .header__content img {
                padding-left:270px;
                padding-top:30px;
                padding-bottom:30px;
                max-width: 50%;
                height: 400px;
              }
              h1{
                font-size:40px;
              }
              
        `}
         </style>
    </>
  );
}

export default Header;
