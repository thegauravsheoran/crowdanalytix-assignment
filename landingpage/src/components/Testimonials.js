import React from 'react';

const Testimonials = () => {
  return (
    <>
    <div className="testimonials" id="testimonials">
      <div className="testimonial">
        <img src="https://evrycard.no/wp-content/uploads/2017/03/testy3-1.png" alt="Person 1" className="testimonial__image" />
        <h3 className="testimonial__name">John Doe</h3>
        <p className="testimonial__position">CEO, Acme Inc.</p>
        <p className="testimonial__quote">"This is the best mobile phone and headset combination I have ever used. The sound quality is exceptional and the design is beautiful."</p>
      </div>

      <div className="testimonial">
        <img src="https://pinotmasters.sk/wp-content/uploads/2015/04/speaker-1-v2.jpg" alt="Person 2" className="testimonial__image" />
        <h3 className="testimonial__name">Jane Smith</h3>
        <p className="testimonial__position">Marketing Director, XYZ Corp.</p>
        <p className="testimonial__quote">"I am blown away by the features of this mobile phone and headset. The Bluetooth compatibility is seamless and the battery life is amazing."</p>
      </div>

      <div className="testimonial">
        <img src="http://afernandes.adv.br/wp-content/uploads/Team-Member-3.jpg" alt="Person 3" className="testimonial__image" />
        <h3 className="testimonial__name">David Lee</h3>
        <p className="testimonial__position">Software Engineer, Google</p>
        <p className="testimonial__quote">"I highly recommend this mobile phone and headset to anyone who wants to experience the latest technology. The user interface is intuitive and the sound quality is top-notch."</p>
      </div>
    </div>
    <style>{`.testimonials {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
    }
    
    .testimonial {
      width: 100%;
      max-width: 350px;
      margin-bottom: 30px;
      padding: 20px;
      border-radius: 5px;
      box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
      background-color: #fff;
      text-align: center;
    }
    
    .testimonial__image {
      width: 120px;
      height: 120px;
      border-radius: 50%;
      margin: 0 auto 20px;
    }
    
    .testimonial__name {
      font-size: 20px;
      font-weight: bold;
      margin-bottom: 5px;
    }
    
    .testimonial__position {
      font-size: 14px;
      margin-bottom: 20px;
    }
    
    .testimonial__quote {
      font-style: italic;
    }
    
    `}</style>
    </>
  );
}

export default Testimonials;
