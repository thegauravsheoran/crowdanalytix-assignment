import React from 'react';


const CallToAction = () => {
  return (
    <>
    <div className="call-to-action" id="call-to-action">
      <p className="call-to-action__text">Get yours today and enjoy the latest technology.</p>
      <a href="#" className="call-to-action__button">Order Now</a>
    </div>
    <style>
        {`
        .call-to-action {
          display: flex;
          justify-content: center;
          align-items: center;
          background-color: #2196f3;
          color: #fff;
          padding: 50px;
          text-align: center;
        }
        
        .call-to-action__text {
          font-size: 24px;
          margin-right: 10px;
        }
        
        .call-to-action__button {
          display: inline-block;
          padding: 10px 20px;
          background-color: #fff;
          color: #2196f3;
          text-decoration: none;
          border-radius: 5px;
        }
        .call-to-action__button:hover{
          background-color:#ECF2FF;
        }
        `}
        </style>
    </>
  );
}

export default CallToAction;
