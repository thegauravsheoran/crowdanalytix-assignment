import { useState } from 'react';
import { Table, Button, Modal } from 'react-bootstrap';
import EmployeeForm from './components/EmployeeForm';
import Employee from './components/Employee';

const employeesData = [
  {
    id: '1',
    name: 'John Doe',
    age: '30',
    department: 'Marketing',
    bloodGroup: 'O+',
    address: '123 Main Street, Anytown USA',
    contactNumber: '555-1234',
  },
  {
    id: '2',
    name: 'Jane Doe',
    age: '35',
    department: 'Sales',
    bloodGroup: 'A-',
    address: '456 Elm Street, Anytown USA',
    contactNumber: '555-5678',
  },
];

const App = () => {
  const [employees, setEmployees] = useState(employeesData);
  const [editingEmployee, setEditingEmployee] = useState(null);
  const [showModal, setShowModal] = useState(false);

  const addEmployee = (employee) => {
    setEmployees([...employees, employee]);
  };

  const deleteEmployee = (id) => {
    setEmployees(employees.filter((employee) => employee.id !== id));
  };

  const updateEmployee = (updatedEmployee) => {
    setEmployees(
      employees.map((employee) =>
        employee.id === updatedEmployee.id ? updatedEmployee : employee
      )
    );
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setEditingEmployee(null);
  };

  const handleShowModal = () => setShowModal(true);

  return (
    <>
      <h1>Employee Management System</h1>
      <Button variant="primary" onClick={handleShowModal}>
        Add Employee
      </Button>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Department</th>
            <th>Blood Group</th>
            <th>Address</th>
            <th>Contact Number</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {employees.map((employee) => (
        <Employee
          key={employee.id}
          employee={employee}
          deleteEmployee={deleteEmployee}
          setEditingEmployee={setEditingEmployee}
        />
      ))}
    </tbody>
  </Table>
  <Modal show={showModal} onHide={handleCloseModal}>
    <Modal.Header closeButton>
      <Modal.Title>{editingEmployee ? 'Edit' : 'Add'} Employee</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <EmployeeForm
        addEmployee={addEmployee}
        updateEmployee={updateEmployee}
        editingEmployee={editingEmployee}
        handleCloseModal={handleCloseModal}
      />
    </Modal.Body>
  </Modal>
</>
);
};

export default App;


