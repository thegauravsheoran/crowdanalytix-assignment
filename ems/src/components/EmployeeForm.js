import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

const EmployeeForm = ({ addEmployee }) => {
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [department, setDepartment] = useState('');
  const [bloodGroup, setBloodGroup] = useState('');
  const [address, setAddress] = useState('');
  const [contactNumber, setContactNumber] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    const newEmployee = {
      id: uuidv4(),
      name,
      age,
      department,
      bloodGroup,
      address,
      contactNumber,
    };
    addEmployee(newEmployee);
    setName('');
    setAge('');
    setDepartment('');
    setBloodGroup('');
    setAddress('');
    setContactNumber('');
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Form.Group controlId="name">
        <Form.Label>Name</Form.Label>
        <Form.Control
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="age">
        <Form.Label>Age</Form.Label>
        <Form.Control
          type="number"
          value={age}
          onChange={(e) => setAge(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="department">
        <Form.Label>Department</Form.Label>
        <Form.Control
          type="text"
          value={department}
          onChange={(e) => setDepartment(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="bloodGroup">
        <Form.Label>Blood Group</Form.Label>
        <Form.Control
          type="text"
          value={bloodGroup}
          onChange={(e) => setBloodGroup(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="address">
        <Form.Label>Address</Form.Label>
        <Form.Control
          type="text"
          value={address}
          onChange={(e) => setAddress(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="contactNumber">
        <Form.Label>Contact Number</Form.Label>
        <Form.Control
          type="tel"
          value={contactNumber}
          onChange={(e) => setContactNumber(e.target.value)}
          required
        />
      </Form.Group>

      <Button variant="primary" type="submit">
        Add Employee
      </Button>
    </Form>
  );
};

export default EmployeeForm;
