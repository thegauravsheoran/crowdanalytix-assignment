import { Button } from 'react-bootstrap';

const Employee = ({ employee, deleteEmployee, setEditingEmployee }) => {
  return (
    <tr>
      <td>{employee.name}</td>
      <td>{employee.age}</td>
      <td>{employee.department}</td>
      <td>{employee.bloodGroup}</td>
      <td>{employee.address}</td>
      <td>{employee.contactNumber}</td>
      <td>
        <Button
          variant="primary"
          onClick={() => setEditingEmployee(employee)}
        >
          Edit
        </Button>
        <Button
          variant="danger"
          onClick={() => deleteEmployee(employee.id)}
        >
          Delete
        </Button>
      </td>
    </tr>
  );
};

export default Employee;
